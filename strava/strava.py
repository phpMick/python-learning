from stravalib.client import Client
import os


class Strava():

    def __init__(self):
        token = os.getenv("STRAVA_TOKEN")
        self.client = Client(access_token=token)


    def get_activities(self):
        activities = self.client.get_activities(limit=1000)
        return activities

