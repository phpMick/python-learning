from dotenv import load_dotenv
import os
from strava import Strava
from stravalib.client import Client
import pandas as pd
from stravalib import unithelper

load_dotenv()

#https://www.patricksteinert.de/wordpress/2017/11/29/analyzing-strava-training
#maybe?

client_id = os.getenv("STRAVA_CLIENT_ID")
client_secret = os.getenv("STRAVA_CLIENT_SECRET")

client = Client()



url = client.authorization_url(
    client_id=client_id,
    redirect_uri='http://localhost/'
)
print(url)

#%%

code = '32373c130814957d98e1bfdf504113ce5a637e98' # Change this to what you see

access_token = client.exchange_code_for_token(
    client_id=client_id,
    client_secret=client_secret,
    code=code
)

refresh_token = access_token['access_token']
client = Client(access_token=refresh_token)# Test the connection
athlete = client.get_athlete()
print(f'Hello, {athlete.firstname}, I know you.')


runs_2019 = pd.DataFrame(
    columns=[
        'date', 
        'activity_id', 
        'moving_time',
        'distance'
    ]
)

for activity in client.get_activities(
    after="2018-12-31T00:00:00Z",
    before="2019-05-27T00:00:00Z"
):

    if activity.type == 'Run':
        runs_2019 = runs_2019.append(
            {
                'date': activity.start_date_local.date(),
                'activity_id': activity.id, 
                'moving_time': activity.moving_time, 
                'distance': activity.distance
            }, 
            ignore_index=True
        )