
import logging
import threading
import time
import concurrent.futures

#Race conditions can occur when two or more threads access a shared piece of data or resource.


class FakeDatabase:
    def __init__(self):
        self.value = 0
        self._lock = threading.Lock()

    def locked_update(self, name):
        logging.info("Thread %s: starting update", name)
        logging.debug("Thread %s about to lock", name)
        with self._lock:
            logging.debug("Thread %s has lock", name)
            local_copy = self.value
            local_copy += 1
            time.sleep(0.1)
            self.value = local_copy
            logging.debug("Thread %s about to release lock", name)
        logging.debug("Thread %s after release", name)
        logging.info("Thread %s: finishing update", name)


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.DEBUG,
                        datefmt="%H:%M:%S")

    database = FakeDatabase()
    logging.info("Testing update. Starting value is %d.", database.value)

    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        for index in range(2): # update the value twice

            #When you tell your ThreadPoolExecutor to run each thread, you tell it which function to run
            # and what parameters to pass to it: executor.submit(database.update, index).
            executor.submit(database.locked_update, index)

    logging.info("Testing locked update. Ending value is %d.", database.value)

    #Expected result of 2?
    #Threads both get self.value of 0 (because first one is sleeping) , increment and write back

    # Ways to solve
    # Only allow one thread at a time into the read-modify-write - using lock in python
    # only one thread at a time can have the lock
    # using .aquire() and .release()


    #Next:
    #Producer-Consumer Threading