#pg 165

# Remove duplicates with sets
# List = []
# Tuple = ()
# Dictionary = {"a":1, "b":2}
# Set 

"""
thelist = (10.6, 11, 8, 10.6, "two", 7)

fixed = set(thelist) # using the factory function
print(fixed)

distances = {10.6, 11, 8, 10.6, "two", 7}

print(distances)
"""


def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return (time_string)
    
    (mins,secs) = time_string.split(splitter)
    return (mins)

def get_coach_data(filename):
    """read in the file """
    try:
        with open (filename) as f:
            data = f.readline()
        return(data.strip().split(','))
    except IOError as ioerr:
        print('File error: ' + str(ioerr))
        return(None)

sarah = get_coach_data('sarah2.txt')

sarah_data={}
sarah_data['Name'] = sarah.pop(0)
sarah_data['DOB'] = sarah.pop(0)
sarah_data['Times'] = sarah

print(sarah_data['Name'] + "'s fastest times are: " +
        str(sorted(set([sanitize(t) for t in sarah_data['Times']]))[0:3]))


        








    