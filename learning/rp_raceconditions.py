
import logging
import threading
import time
import concurrent.futures

#Race conditions can occur when two or more threads access a shared piece of data or resource.


class FakeDatabase:
    """ FakeDatabase is keeping track of a single number: .value. This is going to be the shared data on which you’ll see the race condition."""

    def __init__(self):
        self.value = 0

    def update(self, name):
        """Reads value and writes it back"""
        logging.info("Thread %s: starting update", name)
        local_copy = self.value
        local_copy += 1
        time.sleep(0.1) #allows next thread to start running
        self.value = local_copy
        logging.info("Thread %s: finishing update", name)


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    database = FakeDatabase()
    logging.info("Testing update. Starting value is %d.", database.value)

    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        for index in range(2): # update the value twice

            #When you tell your ThreadPoolExecutor to run each thread, you tell it which function to run
            # and what parameters to pass to it: executor.submit(database.update, index).
            executor.submit(database.update, index)

    logging.info("Testing update. Ending value is %d.", database.value)

    #Expected result of 2?
    #Threads both get self.value of 0 (because first one is sleeping) , increment and write back

    # Ways to solve
    # Only allow one thread at a time into the read-modify-write - using lock in python
    # only one thread at a time can have the lock
    # using .aquire() and .release()


