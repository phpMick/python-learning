#https://realpython.com/intro-to-python-threading/

#Thread is a separate flow of execution - do not actually execute at the same time

#Tasks that spend much of their time waiting for external events are generally good candidates for threading.


import logging
import threading
import time


def thread_function(name):
    logging.info("Thread %s: starting", name)
    time.sleep(10)
    logging.info("Thread %s: finishing", name)


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    logging.info("Main    : before creating thread")

    #when you create a thread, you pass it a function and a list of args
    x = threading.Thread(target=thread_function, args=(1,), daemon=True)


    logging.info("Main    : before running thread")
    x.start()
    logging.info("Main    : wait for the thread to finish")


    x.join() #wait for daemon thread to finish
    logging.info("Main    : all done")

    #Daemon threads
    #Python threading has a more specific meaning for daemon. A daemon thread will shut down immediately when the program exits.
    #If they are not daemons, the program will wait



