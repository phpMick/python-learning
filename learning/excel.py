#https://www.dataquest.io/blog/excel-vs-python/
import pandas as pd


sales = pd.read_csv('sales.csv') #pandas.core.frame.DataFrame
#dataframes can be modified with methods


#sorting
sales =sales.sort_values("Start Date")

#summing (rows)
#columns to sum
q1_columns = sales[["Sales January", "Sales February", "Sales March"]]
sales["Sales Q1"] = q1_columns.sum(axis=1) #tells pandas to sum row


#joining manager data
managers = pd.read_csv('managers.csv')

sales = pd.merge(sales, managers, how='left', on='Department')

#adding conditional - make the Current Employee Column
sales["Current Employee"] = pd.isnull(sales['End Date'])

#pivot tables
sales['Department'].value_counts()

sales.pivot_table(index='Department', values='Sales Q1', aggfunc='mean')


print(sales)

