#https://realpython.com/lessons/type-hinting/

pi: float = 3.142


def greet(name: str) -> str:
    return "Hello, " + name

# MyPy - optional static type checker

def headline(text: str, align: bool = True) -> str:
    if align:       
        return f"{text.title()}\n{'-' * len(text)}"
    else:
        return f" {text.title()} ".center(50, "o")

print(headline("python type checking"))

print(headline("python type checking", align="center"))



headline()
