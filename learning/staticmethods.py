#https://realpython.com/instance-class-and-static-methods-demystified/
class MyClass:

    #This is an instance method
    #Needs to take a parameter self
    #Through self, attributes and methods can be accessed.
    #instance methods can also modify class state
    def method(self):
        return 'instance method called', self
    #has access to the object instance
    

    #This is a class method
    #Takes a cls instead of a self
    #Cannot modify object instance state
    #Can modify class state
    @classmethod
    def classmethod(cls):
        return 'class method called', cls


    #This is a static method
    #Does not take cls or self
    #Cannot modify cls or instance state
    #Primarily away to namespace methods
    @staticmethod
    def staticmethod():
        return 'static method called'


#instance method
obj = MyClass()
print(obj.method())

#class method
print(obj.classmethod())

#static method
print(obj.staticmethod())

#the dot notation passed the cls and self





